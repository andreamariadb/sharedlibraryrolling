#!/usr/bin/env groovy


def checkoutCode() {
    stage("Checking out code repository"){
        checkout scm
    }
}

def readPropertyFile(Map stepParams) {
    config = readProperties file: "${stepParams.configFilePath}"
    return config
}

def rollingDep(Map stepParams){
    cleanWs()

    stage('Cloning Git Repository'){
        try{
            checkoutCode()
        }
        catch(Exception e){
            echo "Unable to clone the Repository"
            echo e.toString()
            //slackSend color: "good", message: "Message from Jenkins Pipeline"
            throw e
        }
    }
        
    stage('Reading config properties'){
        try{
            config = readPropertyFile(
                configFilePath: "${stepParams.configFilePath}"
            )
        }
        catch (Exception e){
            echo "Sorry I'm unable to read Config file"
            echo e.toString()
             //slackSend color: "good", message: "Message from Jenkins Pipeline"
            throw e
        } 
    }

    stage('Rolling Deployment'){
        try{   
            // initialise code 
            // terraform plan
            sh """
                cd Base_infra
                terraform init
                terraform plan
            """
            //Creating version V1 template
            sh """#!/bin/bash
                cd Base_infra
                terraform apply -var="lt_tags={"version" : "1"}" -var="desired_capacity=3" -auto-approve
                sleep 3m
            """   

            //Applying rolling deployment for  upgrading to V2
            sh """#!/bin/bash
                cd Base_infra
                
                for (( i=1; i<=3; i++ ))
                do  
                    terraform apply -var="lt_tags={"version" : "2"}" -var="desired_capacity=4" -var="ami_id=ami-09e67e426f25ce0d7" -auto-approve
                    sleep 3m
                    terraform apply -var="lt_tags={"version" : "2"}" -var="desired_capacity=3" -var="ami_id=ami-09e67e426f25ce0d7" -auto-approve
                    sleep 3m 
                done                
              """        
        }
        catch (Exception e){
            echo "Failed during Rolling Deployment"
            echo e.toString()
            //slackSend color: "good", message: "Message from Jenkins Pipeline"
            throw e
        }
    }

    
    stage('Notification'){
        try {
            //slackSend color: "good", message: "Deployment successful"    
        }
        catch(Exception e){
            echo "Unable to Send Slack notification"
            echo e.toString()
            throw e
        }

    }
    


}


